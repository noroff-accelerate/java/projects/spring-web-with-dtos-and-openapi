package no.accelerate.restserverdemo.models.dtos.project;

import lombok.Data;

@Data
public class ProjectDTO {
    private int id;
    private String title;
}
