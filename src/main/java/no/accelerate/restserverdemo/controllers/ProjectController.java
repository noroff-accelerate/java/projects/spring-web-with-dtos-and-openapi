package no.accelerate.restserverdemo.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.accelerate.restserverdemo.mappers.ProjectMapper;
import no.accelerate.restserverdemo.models.dtos.project.ProjectDTO;
import no.accelerate.restserverdemo.models.dtos.student.StudentDTO;
import no.accelerate.restserverdemo.services.project.ProjectService;
import no.accelerate.restserverdemo.util.ApiErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/v1/projects")
public class ProjectController {
    private final ProjectService projectService;
    private final ProjectMapper projectMapper;

    public ProjectController(ProjectService projectService, ProjectMapper projectMapper) {
        this.projectService = projectService;
        this.projectMapper = projectMapper;
    }

    @Operation(summary = "Get a project by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Project does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id) {
        ProjectDTO proj = projectMapper.projectToProjectDto(
                projectService.findById(id)
        );
        return ResponseEntity.ok(proj);
    }

    @PutMapping("{id}")
    public ResponseEntity updateProject(@RequestBody ProjectDTO projectDTO, @PathVariable int id) {
        if(id != projectDTO.getId())
            return ResponseEntity.notFound().build();
        projectService.update(
                projectMapper.projectDtoToProject(projectDTO)
        );
        return ResponseEntity.noContent().build();
    }
}
