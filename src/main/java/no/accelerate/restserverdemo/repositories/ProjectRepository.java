package no.accelerate.restserverdemo.repositories;

import no.accelerate.restserverdemo.models.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository (DAO) for the Professor domain class.
 * Uses @Query for business logic that is difficult to achieve with default functionality.
 */
@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {
    @Modifying
    @Query("update Student s set s.project.id = ?2 where s.id = ?1")
    void updateProjectById(int studentId, int professorId);
}
